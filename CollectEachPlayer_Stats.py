import re
import os
import commands
from pushbullet import Pushbullet

######### Pysh notification for insert players
def EmailPushNotifier(p1):
	kkey = "o.7wJwK0tazfv2zwqgQJ9bkxTouupJqr0x"

	pb = Pushbullet(kkey)

	if (re.match('.*(error|fatal|failed|fail).*',p1,re.IGNORECASE)):
		push = pb.push_note("Error occured in the insert part -- Players tabe = ERROR : ", p1)
	else:
		push = pb.push_note("Records inserted into Players table", p1)


#### Remove unwanted characters in the list
def ReplaceUnwantedStrings(dataLine):
	dataLine.replace('<','')
	dataLine.replace('/','')
	# dataLine.replace('<','')

	return dataLine

file1 = open('All_2016_Players.html','r')

BB_reference="http://www.basketball-reference.com"
playerName_URL = ""
playerName = ""
os.system('rm Insert_SQL_Playersv1*')

Player_file = open('Insert_SQL_Playersv1.sql', 'ab+')

data_line_Insert='INSERT INTO `nba_2016`.`player_stats`\
	(`Player_name`,`RK`,`Games_played`,`Date`,`Age`,`Team_name`,`H_A`,`OPP_name`,`Win`,`Game_started`,`Min_played`,`Field_goals`,`Field_goalsa`,\
	`Field_goalsp`,`3points`,`3points_a`,`3points_p`,`Free_throws`,`Free_throwsa`,`Free_throwsp`,`ORB`,`DRB`,`TRB`,`AST`,`STL`,`BLK`,`TOV`,`PF`,`PTS`,\
	`GMSC`,`plus_minus`,`DF`)VALUES \n'


Player_file.write(data_line_Insert)

numberofInserts  = 0

for each in file1:
	BB_reference = "http://www.basketball-reference.com"


	if re.match('.*href="/players/', each):
		temp=each.strip('\n')
		#### splitting to extract URL FOR the players
		playerName_URL =  (temp.split('href="')[-1]).split('">')[0]
	  	#print playerName_URL
		#### Extract playerName now
		playerName =  ((temp.split('>')[2]).split('">')[0]).split('<')[0]
		playerName = playerName.replace('\'', ' ')
		#print playerName


		## Now go to that page soo os.system now
		BB_reference = BB_reference + playerName_URL[:-5]+ "/gamelog/2016/"
		print BB_reference
		p = commands.getoutput('curl %s'% (BB_reference))
		#print 	p

		start_collection = 0
		data_temp = '\''+ playerName + '\','
		#Heading for the file
		data_line = ""


		# 'Player_name,Rk,G,Date,Age,Tm,,Opp,,GS,MP,FG,FGA,FG%,3P,3PA,3P%,FT,FTA,FT%,ORB,DRB,TRB,AST,STL,BLK,TOV,PF,PTS,GmSc,+/-,DFS\n'
		for line in p.splitlines():

			#data_line = ""
			#start_collection = 0
			############# start collecting from here 
			if re.match('.*"pgl_basic\.[0-9].*', line):
				start_collection = 1
				data_temp = '(\''+ playerName + '\','
				#print line
			#if start_collection and re.match('<\\tr>',line):
			#	data_temp = ""


			########### collecting the data
			if start_collection and re.match('.*<td.*',line) and not re.match('.*</a></td>.*',line) and not \
			 re.match('.*<span.*',line):
				temp = (line.split('</td')[0]).split('>')[-1]

				if re.match('.*=.*',temp):
					temp = "0"

				temp = '\'' + temp + '\''
				data_temp = data_temp + temp + ','
				#print data_temp

			######### no special cases for collecting different teams
			if start_collection  and re.match('.*</a></td>.*',line):
				temp = (line.split('</a>')[0]).split('>')[-1]

				if re.match('.*=.*',temp):
					temp = "0"
				#print temp
				temp = '\'' + temp + '\''

				data_temp = data_temp +  temp + ','

			######### collect number of games played
			if start_collection and re.match('.*</span>.*',line):
				temp = (line.split('</span>')[0]).split('>')[-1]
				# print temp

				if re.match('.*=.*',temp):
					temp = "0"

				temp = '\'' + temp + '\''

				#print temp
				data_temp = data_temp + temp + ','

			######### end of one game so dump everything to data_line
			if re.match('.*tr>',line) and start_collection:
				start_collection = 0
				#print data_temp
				data_temp = data_temp[:-1]
				if not re.match('.*Inactive.*',data_temp) and not re.match('.*Did.*Not.*Play.*',data_temp) and \
				data_temp.count(',') == 31 :
					data_line = data_line + data_temp + '),' + '\n'

			########### end the player so break out here	
			if re.match('tbody.>*',line) and start_collection:
				break

		#print data_line
		# data_line = data_line_Insert + data_line
		# if not re.match('.*Inactive.*',data_line) and not re.match('.*Did.*Not.*Play.*',data_line):
		data_line = ReplaceUnwantedStrings(data_line)

		Player_file.write(data_line)

		numberofInserts = numberofInserts + 1
		# print numberofInserts
		####### Make sure the number of inserts doest get outof hand do it 15 players at a time.
		if (numberofInserts >= 15):
			Player_file.seek(-2, os.SEEK_END)
			Player_file.truncate()

			Player_file.write(';')
			numberofInserts = 0
			Player_file.write('\n')
			Player_file.write(data_line_Insert)


p1 = commands.getoutput('mysql -h 127.0.0.1 -u "root" "-proot" "nba_2016" < Insert_SQL_Playersv1.sql')
		
EmailPushNotifier(p1)








