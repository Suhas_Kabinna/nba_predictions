import re
import os
import sys
import commands
# import smtplib
# sys.path.insert(0, '/pushbullet.py-0.10.0/pushbullet')

from pushbullet import Pushbullet

#########
def EmailPushNotifier(p1,p2):
	kkey = "o.7wJwK0tazfv2zwqgQJ9bkxTouupJqr0x"

	pb = Pushbullet(kkey)

	if (re.match('.*(error|fatal|failed|fail).*',p1,re.IGNORECASE)) or (re.match('.*(error|fatal|failed|fail).*',p2,re.IGNORECASE)  ):
		push = pb.push_note("Error occured in the insert part-- Teams and Opp table", p1 + p2 )
	else:
		push = pb.push_note("Passed insert into Teams and Opp Table", p1 + p2 )






file1 = open('All_2016_Teams.html','r')

BB_reference="http://www.basketball-reference.com"
teamName_URL = ""
teamName = ""
os.system('rm *\.csv')
os.system('rm TeamStats*')
os.system('rm TeamOppStats*')


Team_file_stats = open('TeamStats.sql', 'ab+')
Team_Oppfile_stats = open('TeamOppStats.sql', 'ab+')


# data_line_Insert = ',G,MP,FG,FGA,FG%,3P,3PA,3P%,2P,2PA,2P%,FT,FTA,FT%,ORB,DRB,TRB,AST,STL,BLK,TOV,PF,PTS'
data_line_Insert = "truncate `nba_2016`.`team_stats`;\n INSERT INTO `nba_2016`.`team_stats` (`Team_name`,`Games_played`,`Min_played`,\
	`Field_goals`,`Field_goalsa`,`Field_goalsp`,`3points`,\
`3points_a`,`3points_p`,`2points`,`2points_a`,`2points_p`,`Free_throws`,`Free_throwsa`,`Free_throwsp`,`ORB`,`DRB`,`TRB`,`AST`,`STL`,\
`BLK`,`TOV`,`PF`,`PTS`) VALUES \n" 

data_lineOpp_Insert = "truncate `nba_2016`.`Opponent_stats`;\n INSERT INTO `nba_2016`.`Opponent_stats` (`Team_name`,`Games_played`,`Min_played`,`Field_goals`,`Field_goalsa`,`Field_goalsp`,`3points`,\
`3points_a`,`3points_p`,`2points`,`2points_a`,`2points_p`,`Free_throws`,`Free_throwsa`,`Free_throwsp`,`ORB`,`DRB`,`TRB`,`AST`,`STL`,\
`BLK`,`TOV`,`PF`,`PTS`) VALUES \n" 

Team_file_stats.write(data_line_Insert)
Team_Oppfile_stats.write(data_lineOpp_Insert)
# EmailPushNotifier("1","2")

for each in file1:
	BB_reference = "http://www.basketball-reference.com"
	if re.match('.*href="/teams/[A-Z].*', each):
		temp=each.strip('\n')
		#### splitting to extract URL FOR the TEAMS
		teamName_URL =  (temp.split('href="')[-1]).split('">')[0]
	  	#print teamName_URL
		#### Extract Teamname now
		teamName =  teamName_URL.split('/')[2]
		#((temp.split('>')[2]).split('">')[0]).split('<')[0]
		#print teamName


		## Now go to that page soo os.system now
		BB_reference = BB_reference + teamName_URL + '2016.html'
		print BB_reference
		# print teamName_URL
		p = commands.getoutput('curl %s'% (BB_reference))
		#print 	p

		start_collection = 0
		start_collection2 = 0
		data_temp = ""
		#Heading for the file 

		### THERE ARE two fields here opponents and Teams and each has to be captured separately  I think.
		## Make two data lines one for Teams and other for Teams
		#data_line='Rk,G,Date,Age,Tm,,Opp,,GS,MP,FG,FGA,FG%,3P,3PA,3P%,FT,FTA,FT%,ORB,DRB,TRB,AST,STL,BLK,TOV,PF,PTS,GmSc,+/-,DFS\n'
		data_line = 'Team_Name,G,MP,FG,FGA,FG%,3P,3PA,3P%,2P,2PA,2P%,FT,FTA,FT%,ORB,DRB,TRB,AST,STL,BLK,TOV,PF,PTS'
		data_line_Team =""
		data_line_Opp = ""

		data_line_gn = ',PW,PL,MOV,SOS,SRS,ORtg,DRtg,Pace,FTr,3PAr,eFG%,TOV%,ORB%,FT/FGA,eFG%,TOV%,DRB%,FT/FGA,Arena,Attendance'
		for line in p.splitlines():


			if re.match('.*div_team_stats.*',line):
				start_collection = 1 
				data_temp = ""
			#	print line

			########### collecting the data
			if start_collection and re.match('.*<td.*',line) and not re.match('.*</a></td>.*',line) and not \
			re.match('.*</span>.*>',line):
				temp = (line.split('</td')[0]).split('>')[-1]
				temp = '\'' + temp + '\''
				# if re.match('.*Team',temp):
					# input()

				data_temp = data_temp + temp + ','
				#print temp
				#print data_temp

			######### collect number of games played
			if start_collection and re.match('.*</span>.*>',line):
				temp = (line.split('</span>')[0]).split('>')[-1]
			#	print temp
				temp = '\'' + temp + '\''
				data_temp = data_temp + temp + ','

			
			######## Start Collection of table
			if re.match('<tr  class="">',line) and start_collection:
                         	#print data_temp
            			if not re.match('.*Team/G.*',data_temp) and not re.match('.*Lg Rank.*',data_temp) and not re.match('.*Opponent/G.*',data_temp) \
            			and not re.match('.*Year.*',data_temp):
				 	data_line = data_line + data_temp + '\n'
				data_temp = ""

			

			########### end the player so break out here	
			if re.match('.*/table.*',line) and start_collection:

				start_collection = 0
            			if not re.match('.*Team/G.*',data_temp) and not re.match('.*Lg Rank.*',data_temp) and not re.match('.*Opponent/G.*',data_temp) \
            			and not re.match('.*Year.*',data_temp):
		                	data_line = data_line + data_temp + '\n'

				#break


			if re.match('.*div_team_misc*',line):
				start_collection2 = 1 
				data_temp = ""

			########### collecting the data
			if start_collection2 and re.match('.*<td.*',line) and not re.match('.*</a></td>.*',line) and not \
			 re.match('.*</span>.*>',line):
				temp = (line.split('</td')[0]).split('>')[-1]
				temp = temp.replace(',','')
				temp = '\'' + temp + '\''
				
				data_temp = data_temp + temp + ','

			######## Start Collection of table
			if re.match('<tr  class="">',line) and start_collection2:
                         	#print data_temp
			 	data_line_gn = data_line_gn + data_temp + '\n'
				data_temp = ""
			

			########### end the player so break out here	
			if re.match('.*/table.*',line) and start_collection2:
				data_line_gn = data_line_gn + data_temp + '\n'
				start_collection2 = 0
				break


		# print data_line
		for dLine  in data_line.splitlines():
			# print dLine
			if re.match('\'Team\',.*', dLine):
				# print teamName_URL.split('/')[2]
				# print dLine[6:]
				# dLine.replace('9',teamName_URL.split('/')[2])

				# print dLine[7:-1] + ')'
				# print teamName_URL.split('/')[2]
				data_line_Team = '(\'' +  teamName_URL.split('/')[2] + '\',' + dLine[7:-1] + '),\n'
				# print data_line_Team
				Team_file_stats.write(data_line_Team)


			if re.match('\'Opponent\',.*', dLine):
				# print teamName_URL.split('/')[2]
				# print dLine
				# dLine.replace('9',teamName_URL.split('/')[2])

				# print dLine[7:-1] + ')'
				# print teamName_URL.split('/')[2]
				data_line_Opp = '(\'' +  teamName_URL.split('/')[2] + '\',' + dLine[11:-1] + '),\n'
				# print data_line_Opp
				Team_Oppfile_stats.write(data_line_Opp)

Team_file_stats.seek(-2,os.SEEK_END)
Team_file_stats.truncate()

Team_Oppfile_stats.seek(-2, os.SEEK_END)
Team_Oppfile_stats.truncate()


p1 = commands.getoutput('mysql -h 127.0.0.1 -u "root" "-proot" "nba_2016" < TeamStats.sql')
p2 = commands.getoutput('mysql -h 127.0.0.1 -u "root" "-proot" "nba_2016" < TeamOppStats.sql')

EmailPushNotifier(p1,p2)


		# data_line = ""

		#print data_line_gn
		# Team_file_stats.write(data_line)
		# Team_file_gn.write(data_line_gn)
	#	Player_file.write(data_line)






