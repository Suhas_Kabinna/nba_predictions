# install.packages("RMySQL")
library(RMySQL)


mydb = dbConnect(MySQL(), user='root', password='root', dbname='nba_2016', host='127.0.0.1')

# dbListTables(mydb)
# dbListFields(mydb, 'some_table')

rs = dbSendQuery(mydb, "select * from team_stats")
Teams = fetch(rs, n=-1)

rs = dbSendQuery(mydb, "select * from player_stats")
Players = fetch(rs, n=-1)
