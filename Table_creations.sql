CREATE TABLE `player_stats` (
  `Player_name` varchar(100) NOT NULL, 

  `Games_played` double DEFAULT NULL,
  `Date` varchar(30) DEFAULT NULL,
  `Age` double DEFAULT NULL,
  `Team_name` varchar(3) NOT NULL, 
  `H_A` varchar(30) default NULL,
  `OPP_name` varchar(3) NOT NULL, 
    `Win` varchar(10) NOT NULL, 

  `Game_started` double DEFAULT NULL,

  `Min_played` double DEFAULT NULL,
  `Field_goals` double DEFAULT NULL,
  `Field_goalsa` double DEFAULT NULL,
  `Field_goalsp` double DEFAULT NULL,
  `3points` double DEFAULT NULL,
  `3points_a` double DEFAULT NULL,
  `3points_p` double DEFAULT NULL,
  `Free_throws` double DEFAULT NULL,
  `Free_throwsa` double DEFAULT NULL,
  `Free_throwsp` double DEFAULT NULL,
  `ORB` double DEFAULT NULL,
  `DRB` double DEFAULT NULL,
  `TRB` double DEFAULT NULL,
  `AST` double DEFAULT NULL,
  `STL` double DEFAULT NULL,
  `BLK` double DEFAULT NULL,
  `TOV` double DEFAULT NULL,
  `PF` double DEFAULT NULL,
  `PTS` double DEFAULT NULL,
    `GMSC` double DEFAULT NULL,
      `plus_minus` varchar(10) NOT NULL,
    `DF` double DEFAULT NULL,

  PRIMARY KEY (`Player_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
